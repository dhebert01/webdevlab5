"use strict";
const myName = document.createElement("h2");
myName.textContent = "David Hebert";
const doc = document.body;
const div = doc.children[1];
doc.insertBefore(myName, div);

const ul = document.createElement("ul");
doc.insertBefore(ul, div);

const myUl = document.getElementsByTagName("ul")[0];
const myInput = document.getElementsByTagName("input")[0];
const myButton = document.getElementsByTagName("button")[0];

const input = document.createElement("input");
input.type = "number";
input.min = "1";
input.max = "150";
input.id = "in";
div.insertBefore(input, myButton);

const budget = document.getElementById("in");
const p = document.createElement("p");
p.textContent = "Budget is $";
doc.insertBefore(p, div);

function btnClicked(event){
  const listItem = myInput.value;
  myInput.value = "";
  const li = document.createElement("li");
  li.textContent = listItem;
  doc.children[2].appendChild(li);
  p.textContent = "Budget is $" + budget.value;
}

myButton.addEventListener("click", btnClicked);

